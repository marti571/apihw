﻿using System;
using System.Collections.Generic;
using System.Net;

using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using DictionaryAPIPage.Models;
namespace DictionaryAPIPage.Models
{
    //var dictionary = DictionaryAPIPage.FromJson(jsonString);
    public partial class DictionaryItem
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }
    public partial class DictionaryItem
    {
        public static DictionaryItem[] FromJson(string json) => JsonConvert.DeserializeObject<DictionaryItem[]>(json, DictionaryAPIPage.Models.Converter.Settings);
    }
   
    public partial class Type
    {
        [JsonProperty("noun")]
        public string Noun { get; set; }
        [JsonProperty("adjective")]
        public string Adjective { get; set; }
        [JsonProperty("verb")]
        public string Verb { get; set; }
        [JsonProperty("adverb")]
        public string Adverb { get; set; }
        [JsonProperty("pronoun")]
        public string Pronoun { get; set; }
        [JsonProperty("preposition")]
        public string Preposition { get; set; }
        [JsonProperty("conjunction")]
        public string Conjunction { get; set; }
        [JsonProperty("determiner")]
        public string Determiner { get; set; }
        [JsonProperty("contraction")]
        public string Contraction { get; set; }
    }
    //public partial class Definition
    //{
    //    public string sentence { get; set; }
    //}
    //public partial class Example
    //{
    //    public string sentence1 { get; set; }
    //}
    public static class Serialize
    {
        public static string ToJson(this DictionaryItem[] self) =>
        JsonConvert.SerializeObject(self,
        DictionaryAPIPage.Models.Converter.Settings);
    }
    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
