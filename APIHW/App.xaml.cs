﻿using Plugin.Connectivity;
using System.Threading.Tasks;
using Xamarin.Forms;
using System;

namespace APIHW
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new APIHWPage());
        }

        protected override void OnStart()
        {
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    MessagingCenter.Send<APIHWPage>(null, "No Internet Connection");
                }
            };
        }
       


        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
