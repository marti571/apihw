﻿using System;
using Plugin.Connectivity;
using System.Threading.Tasks;
using System.Net.Http;
using DictionaryAPIPage.Models;
using Xamarin.Forms;
using System.Linq;

namespace APIHW
{
    public partial class APIHWPage : ContentPage
    {
        public APIHWPage()
        {
            InitializeComponent();

        }

        async void Button_Clicked(object sender, System.EventArgs e)
        {
            if(CrossConnectivity.Current.IsConnected == true)
            {
                string word1 = word.Text;
                string word2=word1.ToLower();


                HttpClient client = new HttpClient();

                var uri = new Uri(
                    string.Format(
                        $"https://owlbot.info/api/v2/dictionary/{word2}"));

                var request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = uri;
                request.Headers.Add("Application", "application / json");

                HttpResponseMessage response = await client.SendAsync(request);
                DictionaryItem[] DictData = null;
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    DictData = DictionaryItem.FromJson(content);
                    if (DictData != null)
                    {
                        DictionaryData.Text = $"Type: {DictData[0].Type}";
                        DictionaryData1.Text = $"Definition: {DictData[0].Definition}";
                        DictionaryData2.Text = $"Example: {DictData[0].Example}";
                    }
                    else if(DictData == null)
                    {
                        bool answer = await DisplayAlert("Error", "Sorry can't find word, try again", "OK", "Cancel");
                        if (answer == true)
                        {
                            await Navigation.PushAsync(new APIHWPage());
                        }
                    }
                }
            }
            else
            {
                bool answer1= await DisplayAlert("Alert", "No internet connection", "OK","Cancel");
                if (answer1 == true)
                {
                    await Navigation.PushAsync(new APIHWPage());
                }
            }
        }
      


    }

}
